# SAE Particules 

Ce projet vise à mettre en place un système de particules. Il s’agit d’un outil fréquemment utilisé
dans les jeux vidéos, les logiciels de 3D, les effets spéciaux pour le cinéma, etc. Un tel système permet
de simuler graphiquement des effets complexe