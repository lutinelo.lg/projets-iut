package assets

import (
	_ "image/png"
	"log"
	"project-particles/config"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

// ParticleImage est une variable globale pour stocker l'image d'une particule
var ParticleImage []*ebiten.Image

// Get charge en mémoire l'image de la particule. (particle.png)
// Vous pouvez changer cette image si vous le souhaitez, et même en proposer
// plusieurs, qu'on peut choisir via le fichier de configuration. Cependant
// ceci n'est pas du tout central dans le projet et ne devrait être fait que
// si vous avez déjà bien avancé sur tout le reste.
func Get() {
	ParticleImage = nil
	for i := 0; i < len(config.General.ParticleImage); i++ {
		img, _, err := ebitenutil.NewImageFromFile(config.General.ParticleImage[i])
		ParticleImage = append(ParticleImage, img)
		if err != nil {
			log.Fatal("Problem while loading particle image: ", err)
		}
	}
}
