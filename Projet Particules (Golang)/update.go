package main

import (
	"math/rand"
	"project-particles/assets"
	"project-particles/config"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

// Update se charge d'appeler la fonction Update du système de particules
// g.system. Elle est appelée automatiquement exactement 60 fois par seconde par
// la bibliothèque Ebiten. Cette fonction ne devrait pas être modifiée sauf
// pour les deux dernières extensions.
func (g *game) Update() error {
	if config.General.SpawnCursor {
		config.General.SpawnX, config.General.SpawnY = ebiten.CursorPosition()
	}
	if ebiten.IsKeyPressed(ebiten.Key1) {
		config.Get("config.json")
		assets.Get()

	}
	if inpututil.IsKeyJustPressed(ebiten.Key2) {
		config.Get("presets/config1.json")
		assets.Get()

	}
	if ebiten.IsKeyPressed(ebiten.Key3) {
		config.Get("presets/config2.json")
		assets.Get()
		config.General.SpawnX = rand.Intn(config.General.WindowSizeX - 50)
		config.General.SpawnY = rand.Intn(config.General.WindowSizeY - 50)

	}
	if ebiten.IsKeyPressed(ebiten.Key4) {
		config.Get("presets/config3.json")
		assets.Get()

	}
	g.system.Update()

	return nil
}
