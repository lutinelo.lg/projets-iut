package particles

import (
	"math"
	"math/rand"
	"project-particles/config"
)

// Update mets à jour l'état du système de particules (c'est-à-dire l'état de
// chacune des particules) à chaque pas de temps. Elle est appellée exactement
// 60 fois par seconde (de manière régulière) par la fonction principale du
// projet.
// C'est à vous de développer cette fonction.
func (s *System) Update() {
	s.Spawnrate()
	//Ces lignes permettent la mise en mouvement des particules//
	for i := 0; i < len(s.Content); i++ {

		if s.Content[i].Vie < config.General.VieMax {

			s.Content[i].Life()

			s.Content[i].PositionX = s.Content[i].PositionX + s.Content[i].SpeedX
			s.Content[i].PositionY = s.Content[i].PositionY + s.Content[i].SpeedY

			if config.General.Rebond {
				s.Rebond(i)
			}
			if config.General.Gravity {
				s.Gravite(i)
			}
			if config.General.ModifyOpacity != "none" {
				s.Content[i].Opacite()
			}
			if config.General.ColorUpdate {
				s.Content[i].UpdateColor()
			}
			if config.General.ScaleModifier {
				s.Content[i].ScaleUdapte()
			}

		}

		if s.Content[i].IsNotVisibleOnScreen() { //Une des particules n'est plus en vie
			s.Content[i].Vie = config.General.VieMax
			j := len(s.Content) - 1
			for s.Content[j].Vie >= config.General.VieMax && j > 1 { //Recherche la dernière particule vivante du tableau
				j--
			}
			s.Content[i], s.Content[j] = s.Content[j], s.Content[i] //Échange de place les paarticules
		}
	}
}

//-----------------------------------------------------------------------//
//							Spawn Particule                              //
//-----------------------------------------------------------------------//
func (s *System) Spawnrate() {
	s.Spawn = s.Spawn + config.General.SpawnRate
	for ; s.Spawn >= 1; s.Spawn-- {
		s.CheckFirstDeathPart()
		s.AddParticules()
	}

}

//-----------------------------------------------------------------------//
//							 Gravité                                     //
//-----------------------------------------------------------------------//
func (s *System) Gravite(i int) {
	s.Content[i].SpeedY -= config.General.GravityValue
}

//-----------------------------------------------------------------------//
//							 Rebond                                      //
//-----------------------------------------------------------------------//
func (s *System) Rebond(i int) {
	var TailleX = s.Content[i].PositionX + s.Content[i].SpeedX
	var TailleY = s.Content[i].PositionY + s.Content[i].SpeedY

	if TailleX > float64(config.General.WindowSizeX) || TailleX < 0 {
		s.Content[i].SpeedX = -(s.Content[i].SpeedX)
	}
	if TailleY > float64(config.General.WindowSizeY) || TailleY < 0 {
		s.Content[i].SpeedY = -(s.Content[i].SpeedY)
	}
}

//-----------------------------------------------------------------------//
//							    Vie                                      //
//-----------------------------------------------------------------------//
func (p *Particle) Life() {
	p.Vie += 1
	if p.IsNotVisibleOnScreen() {
		p.Vie = config.General.VieMax
	}
}

//-----------------------------------------------------------------------//
//							    Opacité                                  //
//-----------------------------------------------------------------------//
func (p *Particle) Opacite() {
	if config.General.ModifyOpacity == "decrease" {
		p.Opacity -= config.General.OpacityModifier
	}
	if config.General.ModifyOpacity == "increase" {
		p.Opacity += config.General.OpacityModifier
	}

}

//-----------------------------------------------------------------------//
//							 Color Modifier                              //
//-----------------------------------------------------------------------//

func (p *Particle) UpdateColor() {

	if config.General.RougeArrivee > p.ColorRed {
		p.ColorRed += config.General.ColorModifierValue
	} else if config.General.RougeArrivee < p.ColorRed {
		p.ColorRed -= config.General.ColorModifierValue
	}

	if config.General.VertArrivee > p.ColorGreen {
		p.ColorGreen += config.General.ColorModifierValue
	} else if config.General.VertArrivee < p.ColorGreen {
		p.ColorGreen -= config.General.ColorModifierValue
	}

	if config.General.BleuArrivee > p.ColorBlue {
		p.ColorBlue += config.General.ColorModifierValue
	} else if config.General.BleuArrivee < p.ColorBlue {
		p.ColorBlue -= config.General.ColorModifierValue
	}

}

//-----------------------------------------------------------------------//
//							    Taille                                   //
//-----------------------------------------------------------------------//
func (p *Particle) ScaleUdapte() {
	p.ScaleX += config.General.ScaleModifierValue
	p.ScaleY += config.General.ScaleModifierValue

}

//-----------------------------------------------------------------------//
//							Optimisation                                 //
//-----------------------------------------------------------------------//
func (p *Particle) IsNotVisibleOnScreen() bool {
	return ((p.PositionX <= -config.General.Marge) || (p.PositionX >= float64(config.General.WindowSizeX)+config.General.Marge) || (p.PositionY <= -config.General.Marge) || (p.PositionY >= float64(config.General.WindowSizeY)+config.General.Marge) || (p.Vie >= config.General.VieMax))
}

//-----------------------------------------------------------------------//
//							Forme		                                 //
//-----------------------------------------------------------------------//
func Cercle(OffsetX, OffsetY float64) (float64, float64) {
	x := float64(rand.Intn(int(config.General.RayonCercleValue)*1000)) / 1000
	y := math.Sqrt((config.General.RayonCercleValue * config.General.RayonCercleValue) - (x * x))
	k := rand.Intn(4)
	switch k {
	case 0:
		return x + OffsetX, y + OffsetY
	case 1:
		return -x + OffsetX, y + OffsetY
	case 2:
		return x + OffsetX, -y + OffsetY
	default:
		return -x + OffsetX, -y + OffsetY
	}
}
