package particles

import (
	"project-particles/config"
	"testing"
)

//Test du nombre de particules//
func TestNbparticles(t *testing.T) {
	config.Get("../config.json")
	System := NewSystem()
	if len(System.Content) != config.General.InitNumParticles {
		t.Error("Il n'y a pas le bon nombre de particules")
	}
}

//Les particules bougent ou non//
func TestSpeedparticles(t *testing.T) {
	var compt int = 0
	config.Get("../config.json")
	System := NewSystem()
	for i := 0; i < len(System.Content); i++ {
		if System.Content[i].SpeedX == 0 && System.Content[i].SpeedY == 0 {
			compt += 1
		}
	}
	if compt > 1 {
		t.Error("Les particules ne bougent pas")
	}

}

//Les particules spawn toutes dans la fenêtre de visionnement//
func TestFenetreparticles(t *testing.T) {
	config.Get("../config.json")
	if config.General.SpawnX > config.General.WindowSizeX || config.General.SpawnY > config.General.WindowSizeY {
		t.Error("Les particules spawn hors de la fenetre de visionnement")
	}
}

//D'autres particules apparaisent bien (en fonction du .json)
func TestSpawnrateparticles(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.Matrix = false
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	nb_frame_test := 10

	if float64(config.General.VieMax) >= float64(nb_frame_test) {
		attendu := float64(nb_frame_test*int(config.General.SpawnRate) + config.General.InitNumParticles)
		for i := 0; i < nb_frame_test; i++ {
			System.Update()
		}
		if float64(len(System.Content)) > attendu+10 {
			t.Error("Le Spawnrate n'est pas respecté")
		}
	} else {
		attendu := float64(config.General.VieMax*int(config.General.SpawnRate) + config.General.InitNumParticles)
		for i := 0; i < nb_frame_test; i++ {
			System.Update()
		}
		if float64(len(System.Content)) > attendu+10 {
			t.Error("Le Spawnrate n'est pas respecté")
		}
	}
}

//Test de notre gravité
func TestGravite(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	if config.General.Gravity {
		for i := 0; i < 10; i++ {
			System.Update()
			for j := 0; j < len(System.Content); j++ {
				if System.Content[i].PositionY < (float64(config.General.SpawnY) - 50) {
					t.Error("La gravité ne fonctionne pas")

				}
			}
		}
	}
}

//Test de notre fonction Rebond
func TestRebond(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	if config.General.Rebond == true {
		for j := 0; j < 10; j++ {
			for i := 0; i < len(System.Content); i++ {
				System.Update()
				if (System.Content[i].PositionX <= 0) || (System.Content[i].PositionX >= float64(config.General.WindowSizeX)) || (System.Content[i].PositionY <= 0) || (System.Content[i].PositionY >= float64(config.General.WindowSizeY)) {
					t.Error("La rebond ne fonctionne pas")
				}
			}
		}
	}
}

//Test de notre marge si elle est fonctionnelle
func TestMarge(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	for i := 0; i < 10; i++ {
		System.Update()
		for j := 0; j < len(System.Content); j++ {
			if (System.Content[i].PositionX <= -config.General.Marge) || System.Content[i].PositionX >= float64(config.General.WindowSizeX)+config.General.Marge || (System.Content[i].PositionY <= -config.General.Marge) || (System.Content[i].PositionY >= float64(config.General.WindowSizeY)+config.General.Marge) {
				t.Error("La particule va au-delà de la marge")
			}
		}
	}
}

//Test de notre opacité
func TestOpacité(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	if config.General.ModifyOpacity == "decrease" || config.General.ModifyOpacity == "increase" {
		for i := 0; i < 10; i++ {
			System.Update()
			for j := 0; j < len(System.Content); j++ {
				if System.Content[i].Opacity == 1 {
					t.Error("L'oppacité ne fonction pas")
				}
			}
		}
	}
}

//Test de notre changement de couleur
func TestCouleurmodif(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	if config.General.ColorUpdate == true {
		for i := 0; i < len(System.Content); i++ {
			System.Update()
			if System.Content[i].ColorRed == config.General.RougeArrivee && System.Content[i].ColorGreen == config.General.VertDepart && System.Content[i].ColorBlue == config.General.BleuArrivee {
				t.Error("Le changement de couleur ne marche pas")
			}
		}
	}
}

//Test de notre changement de taille
func TestTaille(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 5
	config.General.SpawnRate = 10
	config.General.VieMax = 50
	config.General.ParticleImage = []string{"assets/one.png"}
	System := NewSystem()
	TailleX := config.General.ScaleX
	TailleY := config.General.ScaleY
	if config.General.ScaleModifier == true {
		for i := 0; i < 10; i++ {
			System.Update()
			for j := 0; j < len(System.Content); j++ {
				if System.Content[i].ScaleX == TailleX && System.Content[i].ScaleY == TailleY {
					t.Error("Le changement de taille ne marche pas")
				}
			}
		}
	}
}
