package particles

import (
	"math/rand"
	"project-particles/assets"
	"project-particles/config"
	"time"
)

// NewSystem est une fonction qui initialise un système de particules et le
// retourne à la fonction principale du projet, qui se chargera de l'afficher.
// C'est à vous de développer cette fonction.
// Dans sa version actuelle, cette fonction affiche une particule blanche au
// centre de l'écran.

func NewSystem() (s System) {
	//Ces lignes permettent de faire apparaitre le nombre de particule définit dans le fichier .json (IniNumParticles)//
	for i := 1; i <= config.General.InitNumParticles; i++ {
		s.AddParticules()
	}

	return s
}

//Cette fonction permet de créer une nouvelle particule//
func NewPart() (p Particle) {
	p.Reset()
	return p
}

//Cette fonction permet d'initialiser une nouvelle particule//
func (p *Particle) Reset() {
	rand.Seed(time.Now().UTC().UnixNano())
	p.ScaleX = config.General.ScaleX
	p.ScaleY = config.General.ScaleY
	p.Vie = 0
	p.SpeedX = float64(rand.Intn(int((config.General.SpeedX[1]-config.General.SpeedX[0])*1000))+int(config.General.SpeedX[0]*1000)) / 1000
	p.SpeedY = float64(rand.Intn(int((config.General.SpeedY[1]-config.General.SpeedY[0])*1000))+int(config.General.SpeedY[0]*1000)) / 1000
	p.Opacity = config.General.Opacity

	if config.General.RandomSpawn {
		p.PositionX = float64(rand.Intn(config.General.WindowSizeX))
		p.PositionY = float64(rand.Intn(config.General.WindowSizeY))
		p.ColorRed = rand.Float64()
		p.ColorGreen = rand.Float64()
		p.ColorBlue = rand.Float64()
	} else {
		if config.General.Matrix {
			p.PositionX = float64(rand.Intn(config.General.WindowSizeX))
			p.PositionY = 10

		} else if config.General.Cercle {
			p.PositionX, p.PositionY = Cercle(float64(config.General.SpawnX), float64(config.General.SpawnY))
		} else {
			p.PositionX = float64(config.General.SpawnX)
			p.PositionY = float64(config.General.SpawnY)
		}
		p.ColorRed = config.General.RougeDepart
		p.ColorGreen = config.General.VertDepart
		p.ColorBlue = config.General.BleuDepart

		if len(assets.ParticleImage) > 1 {
			p.Image = assets.ParticleImage[rand.Intn(len(config.General.ParticleImage))]
		} else if len(assets.ParticleImage) == 1 {
			p.Image = assets.ParticleImage[0]
		}
	}
}

//Cette fonction permet d'ajouter au tableau une nouvelle particule ou alors de reinitialiser une particule morte//
func (s *System) AddParticules() {
	if len(s.Content) > 0 && (s.Content[s.FirstParMort].Vie >= config.General.VieMax) {
		s.Content[s.FirstParMort].Reset() //Réinitialiser la particule
	} else {
		s.Content = append(s.Content, NewPart()) //Ajouter une nouvelle particule
	}

}

//Cette fonction permet de vérifier la première particule morte du tableau//
func (s *System) CheckFirstDeathPart() {
	j := 0
	for s.Content[j].Vie < config.General.VieMax && j < (len(s.Content)-1) { //Tant que on voit une particule vivante
		j++
	}
	s.FirstParMort = j
}
