package config

// Config définit les champs qu'on peut trouver dans un fichier de config.
// Dans le fichier les champs doivent porter le même nom que dans le type si
// dessous, y compris les majuscules. Tous les champs doivent obligatoirement
// commencer par des majuscules, sinon il ne sera pas possible de récupérer
// leurs valeurs depuis le fichier de config.
// Vous pouvez ajouter des champs et ils seront automatiquement lus dans le
// fichier de config. Vous devrez le faire plusieurs fois durant le projet.
type Config struct {
	WindowTitle              string
	WindowSizeX, WindowSizeY int
	ParticleImage            []string
	Debug                    bool
	InitNumParticles         int
	RandomSpawn              bool
	SpawnX, SpawnY           int
	SpeedX, SpeedY           []float64
	ScaleX, ScaleY           float64
	Gravity                  bool
	GravityValue             float64
	Rebond                   bool
	SpawnRate                float64
	Marge                    float64
	VieMax                   int
	ModifyOpacity            string
	Opacity                  float64
	OpacityModifier          float64
	RougeDepart              float64
	VertDepart               float64
	BleuDepart               float64
	RougeArrivee             float64
	VertArrivee              float64
	BleuArrivee              float64
	ColorUpdate              bool
	ColorModifierValue       float64
	ScaleModifier            bool
	ScaleModifierValue       float64
	SpawnCursor              bool
	RayonCercleValue         float64
	Cercle                   bool
	Matrix                   bool
}

var General Config
