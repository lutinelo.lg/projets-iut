import numpy as np
import random


def list_prime (n) :
    """Cette fonction prend en entrée un entier n et ressort un tableau de tous les entiers
    premiers inférieurs ou égaux a n"""
    tab = []
    for i in range (2, n+1) :
        diviseurs = 0
        for j in range (1, i+1) :
            if i % j == 0 :
                diviseurs += 1
        if diviseurs == 2 :
            tab.append(i)
    return tab


# Cette fonction prend en entrée deux entiers a et b et leur applique l'algorithme 
# d'euclide. Elle ressort le reste de la division euclidienne de a par b
 
def euclid (a, b) :
	q = 0
	r = a
	while r >= b :
		r -= b
		q += 1
	return q, r

#Cette fonction prend en entrée deux entiers a et b et leur applique l'algorithme 
# étendu d'euclide. elle ressort le plus grand commun diviseur et q et v.

def extended_gcd (a, b) :
    a1, b1 = a, b
    u0, u1, v0, v1 = 1, 0, 0, 1
    q, r = euclid(a, b)	
    while r != 0 :
        a, b = b, r
        u2 = u0 - q*u1
        v2 = v0 - q*v1
        u0, v0, u1, v1 = u1, v1, u2, v2
        q, r = euclid(a, b)		
    return b, u1, v1

#Cette fonction permet de créer une clé publique et une clé privée
def key_creation () :
    tab = list_prime(1000)
    p = random.choice(tab)
    q = random.choice(tab)
    n = p*q
    phi = (p-1)*(q-1)
    e = random.randint(2, phi)
    pgcd, _, _ = extended_gcd(phi, e)
    while pgcd != 1 :
        e = random.randint(2, phi)
        pgcd, _, _ = extended_gcd(phi, e)
    _, _, d = extended_gcd(phi, e)
    priv = d % phi
    return n, e, priv


#On convertit le message en ASCII
def convert_msg (msg) :
    res = ""
    for i in range(0,len(msg)) :
        if ord(msg[i]) < 100 :
            res += "0"
        res += str(ord(msg[i]))
    print("Le message convertit en ASCII est:",res)

    while len(res) % 4 != 0:
        res += "0" 
    tab = [""] * int((len(res)/4))
    j = 0
    for i in range (3, len(res), 4) :
        tab[j] = res[i-3:i+1]
        j += 1
    
    return tab

#Fonction pour repasser en lettres à partir de caractères ASCII
def deconvert (msg) :
    res = ""
    for i in range(0,len(msg)) :
        res += msg[i]
    tab = []
    for i in range(3, len(res), 3) :
        tab.append(res[i-3:i])
    print("Message décrypté avec la clé privée", tab)
    for i in range(0, len(tab)) : 
        tab[i] = chr(int(tab[i]))
    tmp = ""
    for i in range(0, len(tab)) :
        tmp += tab[i]
    return tmp

#Fonction d'encryptage
def encryption(n, pub, msg) :
    tmp = convert_msg(msg)
    for i in range (0, len(tmp)) :
        tmp[i] = (int(tmp[i]) ** pub) % n
    return tmp

#Fonction de décryptage
def decryption(n, priv, msg) :
    tmp = msg
    for i in range (0, len(tmp)) :
        tmp[i] = str((int(tmp[i]) ** priv) % n)
        while len(tmp[i]) < 4 :
            tmp[i] = "0" + tmp[i] 
    return deconvert(tmp)


def convertbin (msg):
    #On converti le message en binaire
    convert = []
    convert2=[]
    convert4 = []
    convert3 = []
    for i in range (0, len(msg)):
    

        temp = format(msg[i], "b")
        convert.append(temp)

        while len(convert[i])%4 != 0:

            convert[i] = "0"  + convert[i]
        h, f = 0, 4
        while h <= len(convert[i])-1:
            convert2.append(convert[i][h:f])
            h,f = f, f + 4
    print("Message en binaire",convert2)

    val = 0
    k=0
    j, l = 0, 4
    for k in range (0, len(convert2)):
        for val in convert2[k]:
            convert3.append(int(val))
        convert4.append(convert3[j:l])
        j, l = l, l+4

    print("Matrice du message (sur 4 bits)", convert4)
    print("\n")
    return convert4




def convert7bit(mat):
    #On multiplie notre matrice avec M pour avoir une matrice sur 7 bits

    M = np.array([
        [1, 1, 0, 1],
        [1, 0, 1, 1],
        [1, 0, 0, 0],
        [0, 1, 1, 1],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
        ])

    matrice = []
    mat7bit = []
    matfinale = []


    for u in range (0, len(mat)):
        matrice = np.array(mat[u])
        mat7bit = np.matmul(M, matrice)
        for i in range (0, len(mat7bit)):
            if mat7bit[i] == 2:
                mat7bit[i] = 0
            if mat7bit[i] == 3:
                mat7bit[i] = 1
        print("Matrice sur 7 bits:", mat7bit)     
        
        matfinale.append(mat7bit)
    print("\n")       
    return matfinale


def noisetab(vect_msg):
    #Fonction qui permet de bruiter toute la matrice
    tabnoise = []
    for i in vect_msg:
        tabnoise.append(noise(i))
    return tabnoise


def noise(vect_msg):
    """
    prend un vecteur vect_msg et renvoie ce vecteur potentiellement bruite
    """
    ### on fait une copie du vecteur initial
    vect = vect_msg.copy()
    ### une chance sur quatre de ne pas bruiter le vecteur
    test = np.random.randint(0,4)
    if test>0:
        index = np.random.randint(0,np.size(vect))
        vect[index] = (vect[index] +1)%2
    print ("Matrice bruitée (vecteurs par vecteurs):",vect)
    return vect


n, pub, priv = key_creation()

msg = input("Entrez votre message : ")
print("\n")
print("La clé publique est:", n,",",  pub)
c = encryption(n, pub, msg )
print("Le message crypté est: ", c)

matrice=convertbin(c)
vect_msg_1 = convert7bit(matrice)
msg_noise_1 = noisetab(vect_msg_1)


print("\n")
print("La clé privée est:",priv)
d = decryption(n, priv, c)
print("Le message convertit en lettre : ", d)
