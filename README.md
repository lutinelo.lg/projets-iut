# Projets IUT

Voici quelques projets que l'on a pu réaliser au cours de l'année.


# Projet Particules

Ce projet a été codé en Golang.
Il présente un générateur de particules qui peut prendre différentes formes selon les paramètres qu'on lui assimile.
Le fichier READ ME.txt explique en détail les fonctionnalités implémentées, je vous invite à y jeter un coup d'oeil.
Pour l'éxécuter il faut télécharger les fichiers et lancer project-particles.exe

# Projet Site Ergonomique

Codé en HTML/CSS, ce site est destiné à une entreprise de Travaux publics.
Le site se devait d'être en partie ergonomique mais aussi esthétique et pratique.
Malheureusement, dû à quelques problèmes, le site n'est pas responsive (sauf une catégorie).
Vous pouvez accéder à ce site (hébergé pour l'occasion) via le lien suivant ou alors télécharger les fichiers et lancer index.html.
Le lien: trifaulttp.infinityfreeapp.com

# Projet Crypter/Décrypter un message

Codé en python, ce programme est capable de crypter un message donné et de le décrypter. Néanmoins, quelques erreurs minimes persistent.
